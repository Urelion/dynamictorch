package community.julieka.dynamicTorch;

import community.julieka.dynamicTorch.utilities.Validations;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Levelled;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

public class ItemListener implements Listener {

	@EventHandler
	public void itemOnGround (PlayerDropItemEvent dropItemEvent){

		//fehlt der Player ?
		Item item = dropItemEvent.getItemDrop();
		ItemStack thrownItem = item.getItemStack();
		Block targetblock = item.getLocation().getBlock();
		if(LightHandler.getLightlevel(thrownItem)>0
		&& Validations.isTargetBlockAir(targetblock)){
			targetblock.setType(Material.LIGHT);

			BlockData targetData = targetblock.getBlockData();
			if (targetData instanceof Levelled) {
				Levelled targetLevelled = (Levelled) targetData;
				targetLevelled.setLevel(LightHandler.getLightlevel(thrownItem));
				targetblock.setBlockData(targetLevelled);
			}
		}


		//LightHandler.lit();
	}
}
