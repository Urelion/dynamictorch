package community.julieka.dynamicTorch;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Singleton;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;


/**
 *
 */

@Singleton
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PlayerListener implements Listener {
	public static final PlayerListener INSTANCE = new PlayerListener();

	@EventHandler
	public void onMove(final PlayerMoveEvent moveEvent) {
		Player player = moveEvent.getPlayer();
		LightHandler.lit(player);
	}
	@EventHandler
	public void onMainHandChange(PlayerItemHeldEvent event) {
		Player player = event.getPlayer();
		PlayerInventory inventory = player.getInventory();
		ItemStack item = inventory.getItem(event.getNewSlot());
		if (item != null) {
			LightHandler.lit(player, item);
		}
	}
}
