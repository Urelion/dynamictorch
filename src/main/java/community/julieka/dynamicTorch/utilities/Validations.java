package community.julieka.dynamicTorch.utilities;

import community.julieka.dynamicTorch.DynamicTorchPlugin;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.type.Light;
import org.bukkit.entity.Player;

/**
 * contains validation methods deciding if {@link Light} is set and which light level.
 * @since 1.0.0
 */
public class Validations {

	/**
	 * validates that only {@link Block}s of type Air are replaced by {@link Light}
	 * @param targetBlock investigated {@link Block} that might be replaced
	 * @return is investigated {@link Block} {@link Material#AIR}
	 * @since 1.0.0
	 */
	public static boolean isTargetBlockAir(Block targetBlock){
		return ((targetBlock.getType() == Material.AIR) || (targetBlock.getType() == Material.CAVE_AIR));
	}
	//TODO Methode ggf umbauen sodass bei potenziell neuen Air Types nicht unendlich viele "||" notwendig sind -> liste

	public static boolean isVisible(Player player) {
		if(DynamicTorchPlugin.isVanishAvailable() && DynamicTorchPlugin.getVanish().getManager().isVanished(player))
			return false;
		return player.getGameMode() != GameMode.SPECTATOR;
	}

	/**
	 * changes {@link Light}level of certain sources if {@link Player#isSprinting()}
	 * @param player player to investigate
	 * @return int value with influence on {@link Light}
	 * @since 1.0.0
	 */
	public static int isLightEffected (Player player) {
		if (player.isSprinting()) {
			switch (player.getInventory().getItemInMainHand().getType()) {
				case TORCH:
					return -6;
				case REDSTONE_TORCH:
					return 4;
			}
		}
		return 0;
	}
}
