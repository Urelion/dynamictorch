package community.julieka.dynamicTorch;

import community.julieka.dynamicTorch.utilities.Validations;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Levelled;
import org.bukkit.block.data.type.Light;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import static community.julieka.dynamicTorch.DynamicTorchPlugin.blocksToDelete;

public class LightHandler {

	public static void lit (Player player, ItemStack item) {
		Block targetBlock = player.getEyeLocation().getBlock();

		int mainLight = getLightlevel(item);
		int offLight = getLightlevel(player.getInventory().getItemInOffHand());
		int lightlevel = 0;
		BlockData targetData = targetBlock.getBlockData();

		lightlevel = Math.max(mainLight, offLight);

		if ((Validations.isTargetBlockAir(targetBlock)) &&
			(lightlevel > 0) && Validations.isVisible(player)){
			blocksToDelete.put(targetBlock.getLocation(), targetBlock.getType());
			targetBlock.setType(Material.LIGHT, false);


			if (targetData instanceof Levelled) {
				Levelled targetLevelled = (Levelled) targetData;
				targetLevelled.setLevel(lightlevel); // + Validations.isLightEffected(player));
				targetBlock.setBlockData(targetLevelled);
			}
		}
	}

	public static void lit(Player player) {
		ItemStack mainItem = player.getInventory().getItemInMainHand();
		LightHandler.lit(player, mainItem);
	}

	/**
	 * checks if {@link Player} holds an Item which should produce {@link Light}
	 * @param item player to get the light level from its hand
	 * @return int value {@link Light} level
	 * @since 1.1.0
	 */
	public static int getLightlevel(ItemStack item){
		switch (item.getType()) {
			case OCHRE_FROGLIGHT:
			case PEARLESCENT_FROGLIGHT:
			case VERDANT_FROGLIGHT:
			case GLOWSTONE:
			case JACK_O_LANTERN:
			case SEA_LANTERN:
			case SHROOMLIGHT:
				return 15;
			case CAVE_VINES:
			case END_ROD:
			case TORCH:
				return 14;
			case LANTERN:
			case SOUL_LANTERN:
			case SOUL_TORCH:
				return 10;
			case REDSTONE_TORCH:
				return 7;
			case AMETHYST_CLUSTER:
				return 5;
			case LARGE_AMETHYST_BUD:
				return 4;
			case MAGMA_BLOCK:
				return 3;
			case MEDIUM_AMETHYST_BUD:
				return 2;
			case SMALL_AMETHYST_BUD:
				return 1;
		} return 0;
	}

	public static int getLightlevel(Player player){
		PlayerInventory inventory = player.getInventory();
		ItemStack mainItem = inventory.getItemInMainHand();
		ItemStack offItem = inventory.getItemInOffHand();
		int mainLight = LightHandler.getLightlevel(mainItem);
		int offLight = LightHandler.getLightlevel(offItem);
		int lightlevelToReturn = Math.max(mainLight, offLight);
		return lightlevelToReturn;
	}

	//TODO Licht unter Wasser

}
